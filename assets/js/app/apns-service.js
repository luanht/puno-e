(function() {
  angular.module('app')
    .factory('APNSService', ['$http', APNSService]);

  function APNSService($http) {
    return {
      send: function(env, pfx, deviceTokens, messageTitle, data) {
        var apn = require('apn');
        if (data == undefined) {
          data = {};
        }

        var connectOpts = {
          pfx: pfx,
          production: env === 'production'
        };

        var connection = new apn.Connection(connectOpts);
        var devices = [];
        for (var i = deviceTokens.length - 1; i >= 0; i--) {
          devices.push(new apn.Device(deviceTokens[i]));
        };

        var msg = new apn.Notification();
        msg.alert = messageTitle;
        msg.payload = data;
        connection.pushNotification(msg, devices);
      }
    }
  }
})();