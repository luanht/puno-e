(function() {
  angular.module('app')
    .factory('GCMService', ['$http', GCMService]);

  function GCMService($http) {
    return {
      send: function(apiKey, deviceTokens, messageTitle, data) {
        var gcm = require('node-gcm');
        if (data == undefined) {
          data = {};
        }

        var msg = new gcm.Message({
          data: data,
          notification: {
            title: messageTitle
          }
        });

        var sender = new gcm.Sender(apiKey);

        sender.send(msg, { registrationTokens: deviceTokens }, function(err, response) {
          if (err) {
            console.error(err);
          } else {
            console.log(response);
          }
        });
      }
    }
  }

})();