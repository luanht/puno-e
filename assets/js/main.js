  angular.module('app', ['ui.ace', 'toastr']);

  angular.module('app')
    .controller('AppCtrl', ['$scope', 'toastr', 'GCMService', 'APNSService', AppCtrl]);

  function AppCtrl($scope, toastr, GCMService, APNSService) {

    $scope.isBusy = false;

    $scope.provider = 'gcm';

    $scope.aceDataConfig = {
      useWrapMode: false,
      showGutter: false,
      mode: 'json'
    };

    $scope.gcmProfile = {
      apiKey: '',
    };

    $scope.apnsProfile = {
      env: 'development',
      certPath: null
    };

    $scope.notif = {
      message: 'Hello ',
      deviceTokens: [''],
      data: ''
    };

    var getData = function() {
      if ($scope.notif.data && $scope.notif.data.trim().length) {
        return JSON.parse($scope.notif.data);
      } else {
        return {};
      }
    };

    var noticeSent = function() {
      toastr.success('It was sent, really! :)');
    };

    $scope.push = function() {
      if ($scope.provider == 'gcm') {
        $scope.isBusy = true;
        GCMService.send(
          $scope.gcmProfile.apiKey,
          $scope.notif.deviceTokens,
          $scope.notif.message,
          getData()
        );
        $scope.isBusy = false;
        noticeSent();
      } else {
        $scope.isBusy = true;
        APNSService.send(
          $scope.apnsProfile.env,
          $scope.apnsProfile.certPath,
          $scope.notif.deviceTokens,
          $scope.notif.message,
          getData()
        );
        $scope.isBusy = false;
        noticeSent();
      }
    };

    $scope.openSelectCertFileDialog = function() {
      var dialog = require('electron').remote.dialog;
      var files = dialog.showOpenDialog({ properties: [ 'openFile' ]});
      if (files) {
        $scope.apnsProfile.certPath = files[0];
      } else {
        $scope.apnsProfile.certPath = null;
      }
    };
  }

